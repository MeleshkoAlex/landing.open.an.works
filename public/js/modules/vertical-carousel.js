export default class VerticalCarousel {
    constructor(props){
        this.slider = props.slider && document.getElementById(props.slider);
        this.idWorkspace = props.workspace;
        this.workspace = props.workspace && document.getElementById(props.workspace);
        this.content = props.content && document.getElementById(props.content);
        this.nextBtn = props.nextBtn && document.getElementById(props.nextBtn);
        this.prevBtn = props.prevBtn && document.getElementById(props.prevBtn);
        this.blockDecision = props.decision && document.getElementById(props.decision);
        this.numberBlock = props.decision && document.getElementById(props.number);
        this.decisionItem = this.blockDecision && this.blockDecision.children;
        this.items = this.workspace && [].slice.call(this.workspace.children) || [];
        this.caption = props.caption && document.getElementById(props.caption);
        this.captionChildren = props.caption && [].slice.call(this.caption.children) || [];
        this.indexAdd = false;
        this.translationComplete = true;
        this.prevIndex = 1;
        this.index = 1;
        this.init();
    }
    init(){
        this.initHeightItem();
        this.initCaptionSlide();
        this.calculation();
        this.setHeightItem();
        this.initWorkspace();
    }
    calculation(){
        let height = this.content.clientHeight;
        this.itemCount = Math.floor(height / (this.maxHeight + 30)) > this.items.length ? this.items.length : Math.floor(height / (this.maxHeight + 30));
        this.fullheight = (height - (15 * this.itemCount)) / this.itemCount;
        this.fullsheight = (height - (15 * this.itemCount)) / this.itemCount;
    }
    initHeightItem(){
        let arrHeight = [];
        this.items.forEach(element => {
            arrHeight.push(element.clientHeight);
        });
        this.maxHeight =  Math.max.apply(Math, arrHeight);
    }
    setHeightItem(){
        this.items.forEach(element => {
            element.style.height = `${this.fullsheight}px`;
        });
    }
    transitionCompleted = ()=>{
        if(this.indexAdd){
            this.prevEndAnim();
        }else{
            this.nextEndAnim();
        }
    }
    initCaptionSlide(){
        let arrHeight = [];
        this.captionChildren.forEach(element=>{
            arrHeight.push(element.clientHeight);
        });
        let height = Math.max.apply(Math, arrHeight);
        this.caption.style.height = `${height}px`;
    }
    nextEndAnim(){
        this.workspace.removeChild(this.workspace.firstElementChild);
        this.workspace.style.cssText = 'transform:translateY(0px); transition: none';
        this.workspace.children[this.itemCount - 1].classList.add('slider-platform__item--active');
        this.prevIndex = this.index;
        this.index = this.workspace.children[this.itemCount - 1].getAttribute('data-title');
        this.decisionActive();
        setTimeout(() => {
            this.translationComplete = true;
        }, 250);
    }
    prevEndAnim(){
        this.workspace.removeChild(this.workspace.lastElementChild);
        this.workspace.style.cssText = 'transform:translateY(0px); transition: none';
        this.workspace.children[this.itemCount - 1].classList.add('slider-platform__item--active');
        this.prevIndex = this.index;
        this.index = this.workspace.children[this.itemCount - 1].getAttribute('data-title');
        this.decisionActive();
        setTimeout(() => {
            this.translationComplete = true;
        }, 250);
    }
    decisionActive(){
        this.numberBlock.innerHTML = `<span>${this.index}</span>`;
        this.captionChildren[this.prevIndex - 1].classList.remove('block-why-we-desc__caption-level-item--active');
        this.captionChildren[this.index - 1].classList.add('block-why-we-desc__caption-level-item--active');
        this.decisionItem[this.prevIndex - 1].classList.remove('block-why-we-desc__level-desc-item--active');
        this.decisionItem[this.index - 1].classList.add('block-why-we-desc__level-desc-item--active');
    }
    initWorkspace =()=>{
        this.amount = this.items.length;
        this.moveOffset = this.fullheight;
        this.prevBtn.addEventListener('click', this.prev);
        this.nextBtn.addEventListener('click', this.next);
        this.initItemOrder();
    }
    initItemOrder(){
        for(let i = 1;this.itemCount > i;i++){
            let clone = this.workspace.lastElementChild.cloneNode(true);
            this.workspace.removeChild(this.workspace.lastElementChild);
            this.workspace.insertBefore(clone, this.workspace.firstElementChild);
        }
       setTimeout(() => {
        this.workspace.children[this.itemCount - 1].classList.add('slider-platform__item--active')
       }, 0);
    }
    deleteAllCLassActive(){
        for(let i = 0,len = this.workspace.children.length; i < len; i++){
            this.workspace.children[i].classList.remove('slider-platform__item--active');
        }
    }
    next = ()=>{
        if(this.translationComplete === true){
            this.translationComplete = false;
            this.deleteAllCLassActive();
            let clone = this.workspace.firstElementChild.cloneNode(true);
            this.workspace.appendChild(clone);
            setTimeout(() => {
                this.workspace.style.cssText = 'transform:translateY(-'+ this.fullheight + 'px); transition: transform 0.5s linear';
                this.indexAdd = false;
            }, 0);
            setTimeout(() => {
                this.transitionCompleted();
            }, 500);
        }
    }
    prev = ()=>{
        if(this.translationComplete === true){
            this.translationComplete = false;
            this.deleteAllCLassActive();
            let clone = this.workspace.lastElementChild.cloneNode(true);
            this.workspace.insertBefore(clone,this.workspace.firstElementChild);
            this.workspace.style.cssText = 'transform:translateY(-'+ this.fullheight + 'px); transition: none';
            setTimeout(() => {
                this.workspace.style.cssText = 'transform:translateY(0px); transition: transform 0.5s linear';
                this.indexAdd = true;
            }, 30);
            setTimeout(() => {
                this.transitionCompleted();
            }, 500);
        }
    }
}