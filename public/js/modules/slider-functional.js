import {createTag} from "../helper/helper.js"
import {Animation} from "../common/anim.js"

export default class Slider {
    constructor(props){
        this.blockDots = props.blockDots;
        this.blockMain = props.blockMain;
        this.blockCaption = props.blockCaption;
        this.imgSlider = document.querySelector(`.${props.imgSlider}`);
        this.dotsArray = [];
        this.duration = 1000;
        this.imgArray = [];
        this.mainBlockNode = '';
        this.sliderItem = '';
        this.activeTabs = {
            prev: "0",
            now: "1"
        };
        this.init();
        window.addEventListener('resize',this.mainBlockHeight)
    }
    init(){
        this.preparationBlockMain();
        this.initDots();
        this.initImg();
    }
    heightContainer(){
        let arr = [];
        for(let i = 0, len = this.sliderItem.length; i < len; i++){
            arr.push(this.sliderItem[i].clientHeight)
        }
        arr.sort((a,b)=>(b-a))
        return arr[0];
    }
    mainBlockHeight = ()=>{
        this.mainBlockNode.style.height = `${this.heightContainer()}px`;
    }
    preparationBlockMain(){
        this.mainBlockNode = document.getElementById(this.blockMain);
        this.sliderItem = this.mainBlockNode && this.mainBlockNode.children || 0;
        for(let i = 0,len = this.mainBlockNode && this.mainBlockNode.children.length; i < len; i++){
            this.mainBlockNode.children[i].setAttribute('data-tabs',i+1)
        }
        this.mainBlockHeight();
        this.mainBlockNode.style.visibility = 'visible';
    }
    initDots(){
        let blockDots = document.getElementById(this.blockDots);
        blockDots && blockDots.addEventListener('click', this.tabsControl);
        for(let i = 0; i < this.sliderItem.length; i++){
            let li = createTag('li'),
                spanRight = createTag('span'),
                spanLeft = createTag('span'),
                spanTop = createTag('span');
                spanRight.className = 'block-functional__slider-dots-item--line-right'; 
                spanLeft.className = 'block-functional__slider-dots-item--line-left';
                spanTop.className = 'block-functional__slider-dots-item--line-top';
            if(i == 0){
                li.className = "block-functional__slider-dots-item block-functional__slider-dots-item--active";
            }else{
                li.className = "block-functional__slider-dots-item";
            }
            li.setAttribute('data-tabs',i+1);
            li.appendChild(spanRight);
            li.appendChild(spanLeft);
            li.appendChild(spanTop);
            this.dotsArray.push(li);
            blockDots.appendChild(li);
        }
    }
    initImg(){
        let blockCaption = document.getElementById(this.blockCaption);
        for(let i = 0; i < this.sliderItem.length; i++){
            let div = createTag('div'),
                h2 = createTag('h2'),
                span = createTag('span');
            if(i == 0){
                div.className = `block-functional__slider-img block-functional__slide-${i+1} block-functional__slider-img--active`;
            }else{
                div.className = `block-functional__slider-img block-functional__slide-${i+1}`;
            }
            h2.innerHTML = this.sliderItem[i].getAttribute('data-title');
            h2.className = "block-functional__slider-item-caption";
            span.innerHTML = `0${i+1}`;
            span.className = "block-functional__slider-item-caption-number";
            h2.appendChild(span);
            div.appendChild(h2);
            this.imgArray.push(div);
            blockCaption.appendChild(div);
        }
    }
    tabsControl = event =>{
        let target = event.target,
            tabsNumber = target.getAttribute('data-tabs');
        if(!target.tagName == 'LI' || !tabsNumber || tabsNumber == this.activeTabs.now) return
        this.activeTabs.prev = this.activeTabs.now;
        this.activeTabs.now = tabsNumber;
        this.dotsArray[this.activeTabs.prev - 1].classList.remove('block-functional__slider-dots-item--active')
        target.classList.add('block-functional__slider-dots-item--active');
        this.controlCaption();
        this.controlElement();
    }
    controlCaption(){
        this.imgArray[this.activeTabs.prev - 1].classList.remove('block-functional__slider-img--active')
        this.imgArray[this.activeTabs.now - 1].classList.add('block-functional__slider-img--active')
    }
    controlElement(){
        let prevItem = this.sliderItem[this.activeTabs.prev - 1],
            nowItem = this.sliderItem[this.activeTabs.now - 1];
            nowItem.classList.add('block-functional__slider-item--active');
        new Animation(
            timePassed =>{
                let timeMuliplier = Animation.quadEaseInOut(this.duration, timePassed);
                prevItem.style.opacity = 1 - timeMuliplier;
                nowItem.style.opacity = timeMuliplier;
            },
            this.duration,
            ()=>{
                prevItem.classList.remove('block-functional__slider-item--active');
            }
        );
    }
}