import {clickEvent} from '../helper/event/clickEvent.js';
import {Animation} from '../common/anim.js'
import {
    hideScrollBody,
    addScrollBody,
    htmlContains
} from '../helper/helper.js';

import Hammer from 'hammerjs';

export default class Menu{
    constructor(){
        this.animMenuFLag = false;

        this.humburger = document.getElementById('humburger');
        this.menuHumb = document.getElementById('menu-humb');
        this.menuList = document.getElementById('menu-list');
        this.menuListChild = this.menuList.children;
        this.header = document.getElementById('header');
        this.lineMenuGray = document.querySelectorAll('.menu-humb__line-menu--gray');
        this.wrapMenuBtn = document.getElementById('wrap-menu-btn');
        this.contactUsMenu = document.getElementById('contact-us-menu');
        this.socialMenu = document.getElementById('social-menu');
        this.logo = document.getElementById('logo');
        this.lineMenuBlack = document.querySelectorAll('.menu-humb__line-menu--black');
        this.lineMenuGray = document.querySelectorAll('.menu-humb__line-menu--gray');
        this.init();
    }
    init(){
        let hammer = new Hammer(this.humburger);
        hammer.on('tap',this.animMenu);
    }
    animMenu = ()=>{
        if(this.animMenuFLag) return;
    
        if(this.menuHumb.classList.contains('menu-humb--show')){
            if(htmlContains('mobile') || htmlContains('tablet')){
                addScrollBody();
            }
            this.animMenuHide();
            return;
        }
        if(document.documentElement.classList.contains('tablet') || document.documentElement.classList.contains('desktop')) {
            this.menuShowIpad();
        }
        this.animMenuFLag = true;
        this.header.classList.add('header--open-menu');
        this.menuHumb.classList.add('menu-humb--show');
        this.menuList.style.opacity = '0';
        this.menuList.style.transform = 'translateY(40px)';
        this.wrapMenuBtn.style.opacity = '0';
        this.contactUsMenu.style.opacity = '0';
        this.wrapMenuBtn.style.transform = 'translateY(40px)';
        this.socialMenu.style.opacity = '0';
        this.socialMenu.style.transform = 'translateY(40px)';
        this.logo.style.opacity = '0';
        this.logo.style.transform = 'translateY(18px)';
        for(var i = 0,len = this.lineMenuGray.length; i < len;i++){
            this.lineMenuGray[i].style.width = '0%';
        }
        this.humburger.classList.add('humb--open-menu');
        this.animMenuStart();
    }
    animMenuStart(){
        if(htmlContains('mobile') || htmlContains('tablet')){
            hideScrollBody();
        }
        setTimeout(()=>{
            this.animGrayLine();
        }, 300);       
    }   
    animGrayLine(){
        let duration  = 300,
            duration2  = 500;
        for(let i = 0,len = this.lineMenuGray.length; i < len;i++){
            this.lineMenuGray[i].style.width = '0%';
        }
        for(let l = 0,lent = this.lineMenuBlack.length; l < lent; l++){
            this.lineMenuBlack[l].style.width = '0%';
        }
            
        new Animation(
            (timePassed)=>{
                let timeMuliplier = Animation.linear(duration2, timePassed);
                this.logo.style.opacity = timeMuliplier;
                this.logo.style.transform = 'translateY(' + ( 18 - ( 18 * timeMuliplier)) + 'px)';
            },
            duration2
        )
        new Animation(
            (timePassed)=>{
                var timeMuliplier = Animation.linear(duration, timePassed);
                for(var i = 0,len = this.lineMenuGray.length; i < len;i++){
                    this.lineMenuGray[i].style.width = (100 * timeMuliplier) + '%';
                }
            },
            duration,
            ()=>{
                var duration = 300;
                new Animation(
                    (timePassed)=>{
                        var timeMuliplier = Animation.linear(duration, timePassed);
                        for(var l = 0,lent = this.lineMenuBlack.length; l < lent; l++){
                            this.lineMenuBlack[l].style.width = (100 * timeMuliplier) + '%';
                        }
                    },
                duration,
                ()=>{
                    this.animMenuItem();
                });
        });
    }
    animMenuItem(duration = 300){
        new Animation(
            (timePassed)=>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                this.menuList.style.opacity = timeMuliplier;
                this.menuList.style.transform = 'translateY(' + (40 - (40 * timeMuliplier)) + 'px)';
                this.wrapMenuBtn.style.opacity = timeMuliplier;
                this.wrapMenuBtn.style.transform = 'translateY(' + (40 - (40 * timeMuliplier)) + 'px)';
                this.contactUsMenu.style.opacity = timeMuliplier;
                this.contactUsMenu.style.transform = 'translateY(' + (40 - (40 * timeMuliplier)) + 'px)';
                this.socialMenu.style.opacity = timeMuliplier;
                this.socialMenu.style.transform = 'translateY(' + (40 - (40 * timeMuliplier)) + 'px)';
            },
        duration);
        new Animation(
            (timePassed)=>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                for(let l = 0,lent = this.lineMenuBlack.length; l < lent; l++){
                    this.lineMenuBlack[l].style.width = ( 100 - (100 * timeMuliplier)) + '%';
                }
            },
        duration,
        ()=>{
            this.removeAttMenu();
        });
    }
    removeAttMenu(){
        this.menuList.removeAttribute('style');
        this.wrapMenuBtn.removeAttribute('style');
        this.socialMenu.removeAttribute('style');
        this.logo.removeAttribute('style');
        for(let i = 0,len = this.lineMenuGray.length; i < len;i++){
            this.lineMenuGray[i].removeAttribute('style');
        }
        for(let l = 0,lent = this.lineMenuBlack.length; l < lent; l++){
            this.lineMenuBlack[l].removeAttribute('style');
        }
        this.animMenuFLag = false;
    }
    animMenuHide = ()=>{
            if(this.animMenuFLag) return;
            this.animMenuFLag = true;
            this.humburger.classList.remove('humb--open-menu');
            this.header.classList.remove('header--open-menu');
            this.menuHumb.classList.remove('menu-humb--show');
            let flagOff = ()=>{
                this.animMenuFLag = false;
                this.menuHumb.removeEventListener('transitionend',flagOff);
            }
            this.menuHumb.addEventListener('transitionend',flagOff);
    }
    menuShowIpad(){
        for(let i = 0,len = this.menuListChild.length; i < len;i++){
            this.menuListChild[i].style.opacity = '0';
            this.menuListChild[i].style.top = '-20px';
        }
        setTimeout(()=>{
            this.menuOpen(0);
        },500);
    }
    menuOpen(item){
        let duration = 500;
        if(this.menuListChild.length <= item){
            this.animMenuFLag = false;
            return;
        }
        setTimeout(()=>{
            this.menuOpen(item+1);
            this.menuPos(item);
        },100);
        this.menuListChild[item].style.position = 'relative';
        let k = new Animation(
            (timePassed)=>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                this.menuListChild[item].style.opacity =  timeMuliplier;
            },
            duration,
            ()=>{
                this.menuListChild[item].style.opacity =  '';
                k = null;
        });		
    }
    menuPos(item){
        let duration = 500,
         k = new Animation(
            (timePassed)=>{
                var timeMuliplier = Animation.quadEaseInOut(duration, timePassed);
                this.menuListChild[item].style.top = -20 + ( 20 * timeMuliplier) + 'px';
            },
            duration,
            ()=>{
                this.menuListChild[item].top = '';
                k = null;
        });		
    }
}