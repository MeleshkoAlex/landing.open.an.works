export default function anchor (positionStart,positionEnd){
    var anchor = document.getElementById('anchor');

    if(!anchor) return;

    function currentYPosition() {
        // Firefox, Chrome, Opera, Safari
        if (self.pageYOffset) return self.pageYOffset;
        // Internet Explorer 6 - standards mode
        if (document.documentElement && document.documentElement.scrollTop)
            return document.documentElement.scrollTop;
        // Internet Explorer 6, 7 and 8
        if (document.body.scrollTop) return document.body.scrollTop;
        return 0;
    }

    function elmYPosition(eID) {
        var elm = eID,
            y = elm.offsetTop,
            node = elm;
        while (node.offsetParent && node.offsetParent != document.body) {
            node = node.offsetParent;
            y += node.offsetTop;
        } return y;
    }

    function smoothScroll(eID) {
        var startY = currentYPosition(),
            stopY = elmYPosition(eID),
            distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25),
            leapY = stopY > startY ? startY + step : startY - step,
            timer = 0;
        if (stopY > startY) {
            for ( let i = startY; i < stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( let i = startY; i > stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
    }
    anchor.addEventListener('click',()=>{
        smoothScroll(document.body);
    });
    window.addEventListener('scroll',(event) => {
        let html = document.documentElement,
            body = document.body,
            scrollTop = html.scrollTop || body && body.scrollTop || 0;
            scrollTop -= html.clientTop;

        if(scrollTop > positionStart ){
            anchor.style.display = 'block';
        }else if(scrollTop < positionEnd){
            anchor.style.display = 'none';
        }
    });
}