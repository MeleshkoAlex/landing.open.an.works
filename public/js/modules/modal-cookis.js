import {Animation} from "../common/anim.js"

export default function cookie(){
    let popUpCookie = document.getElementById('pop-up-cookie'),
        self = this,
        animActive = false;
    this.openPopUp = function(){
        if(animActive) return;
        animActive = true;
        let duration = 300;
        new Animation(
            function(timePassed) {
                let timeMuliplier = Animation.linear(duration, timePassed);
                popUpCookie.style.transform = 'translateY('+ (100 - (100 * timeMuliplier)) + '%)';
            },
            duration,
            function(){
                animActive = false;
        });
    };
    
    this.closePopUp = function(agree){
        if(animActive) return;
        if(agree && agree.id == 'btn-agree-cookie'){
            self.setAgreeLocalStorage();
        }
        animActive = true;
        let duration = 300;
        new Animation(
            (timePassed) => {
                let timeMuliplier = Animation.linear(duration, timePassed);
                popUpCookie.style.transform = 'translateY('+ (110 * timeMuliplier) + '%)';
            },
            duration,
            () => {
                animActive = false;
        });
    };
    this.setAgreeLocalStorage = function(){
        if(localStorage.getItem('cookie')) return;
        localStorage.setItem('cookie', 'agree');
    };

    this.popUpOpenFirst = function(){
        if(localStorage.getItem('cookie')) return;
        self.openPopUp();
    };

    setTimeout(function(){
        self.popUpOpenFirst();
    },7000);

    document.getElementById('btn-agree-cookie').addEventListener('click',function(){
        self.closePopUp(this);
    });
    document.getElementById('btn-close-cookie').addEventListener('click',function(){
        self.closePopUp();
    });
}

