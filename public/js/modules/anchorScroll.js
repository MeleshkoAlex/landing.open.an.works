import {Animation} from "../common/anim.js";
import {getCoords} from "../helper/helper.js";

export default class AnchorScroll{
    constructor(props = {}){
        this.block = props.block;
        this.addEventAnchor();
        this.animationNow = false;
        this.duration = props.duration;
    }
    addEventAnchor(){
        let block = document.getElementById(this.block);
        if(!block) return;
        block.addEventListener('click',this.checkElement,false);
    }
    checkElement = event=>{
        let target = event.target;
        if(target.tagName == 'A'){
            event.preventDefault();
            let linkHref = target.href.indexOf("#")
            if(linkHref !== -1){
                this.animScroll(target,linkHref);
            }else return    
        }else return
    }
    animScroll(target,linkHref){
        let section = document.getElementById(target.href.slice(linkHref + 1)),
            startYOffset = pageYOffset,
            coordinateElement = section && getCoords(section).top || 0;
        new Animation(
            timePassed =>{
                let timeMuliplier = Animation.quadEaseInOut(this.duration, timePassed);
                window.scrollTo(0, startYOffset + (coordinateElement * timeMuliplier))
            },
            this.duration
        );
    }
}