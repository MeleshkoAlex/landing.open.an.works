// import {
//     isValidEmailAddress,
//     isValidName,
//     isVAlidPhone
// } from '../helper/form/rule.js'
import _ajax from '../common/ajax.js'
import {
    _createFormData
} from '../helper/form/form.js'
import {Animation} from '../common/anim.js'
import {delStyle} from '../helper/helper.js'
let form = ()=>{
    function postForm(formData) {
        _ajax.ajax("POST", 'php/send.php', _onReqEnd, formData);
    }
    
    var popUpactiv = false,
        usersData = '',
        advertingPopUp = false;
    
    function _onReqEnd(xhr) {
        var res;
    
        try {
            res = JSON.parse(xhr.responseText);
        } catch(e) {
            res = false;
        }
        if (xhr.status === 200 && res.success) {
            if(advertingPopUp){
                advertingPopUp = false;
                hidePopUp(advertising);
                return;
            }
            if(popUpactiv){
                messSentPopUp(res);
                return;
            }
            messSent(res);
        }else{
            if(advertingPopUp){
                advertingPopUp = false;
                hidePopUp(advertising);
                return;
            }
            if(popUpactiv){
                messSentPopUp(res);
                return;
            }
            messSent(res);
        }
    }
    
     
    function getDataObjArr(form) {
        var dataObjArr = '<h2>' + form.getAttribute('name') + '</h2><br>';
        
        for(var i = 0,len = form.elements.length;i < len;i++){
            if(form.elements[i].type == 'submit'){
                continue;
            }
            dataObjArr += '<strong>'+ form.elements[i].name +' </strong> : '+ form.elements[i].value+' <br> ';	
        }
        return dataObjArr;
    }
    var formSubmitItem = null,
        advertising = document.getElementById('advertising');

        function onsubmitHandlerAdverting(event) {
            event.preventDefault();
            var form = this.querySelector('form');
            advertingPopUp = true;
            var btnSubm = form.querySelector('input[type="submit"]');
        
            btnSubm.setAttribute('disabled', '');
            btnSubm.classList.add('disabled-btn');
            formSubmitItem = form;
            var nameMess = form.getAttribute('name');
            if(getDataObjArrAdven(form) == false){
                return;
            }
            var valueMess = getDataObjArrAdven(form) + usersData,
    
             postData = [
                {"key":"subject","value":nameMess},
                {"key":"dataString","value":valueMess}
            ];
            postForm(_createFormData(postData));
        }
    
        function getDataObjArrAdven(form){
            var str = '';
    
            for(var i = 0,len = form.elements.length; i < len;i++){
                if(form.elements[i].checked){
                    str += '<strong>'+ form.elements[i].getAttribute('data-name') +' </strong><br>';
                }
                if(form.elements[i].id == 'rd1' && form.elements[i].checked){
                    str += '<strong>'+  form.elements[1].value +' </strong><br>';
                    str += '<strong>'+  form.elements[2].value +' </strong><br>';
                }else if(form.elements[i].id == 'rd9' && form.elements[i].checked){
                    str += '<strong>'+  form.elements[11].value +'</strong><br>';
                }
                if(form.elements[i].type == 'submit'){
                    continue;
                }
            }
            return str == '' ? false : str;
        }
    
    
    function showAdvertising(){
        showBlackout();
        advertising.style.opacity = '0';
        advertising.classList.add('show-pop-up');
        setTimeout(function(){
            advertising.style.transition = 'opacity 0.3s linear';
            advertising.style.opacity = '1';
        },100);
    }
    
    function onsubmitHandler(event) {
        event.preventDefault();
        var form = this.querySelector('form');
        if(this.id == 'advertising'){
            // validAdverd(form);
        }else{
            validForm(form);
        }
    
        if(form.id == 'form-contact'){
            popUpactiv = true;
        }
        for(var i = 0,len = form.elements.length; i < len;i++){
            if(form.elements[i].classList.contains('no-valid')){
                return false;
            }
        }
        
        var btnSubm = form.querySelector('input[type="submit"]');
    
        btnSubm.setAttribute('disabled', '');
        btnSubm.classList.add('disabled-btn');
        formSubmitItem = form;
        var nameMess = form.getAttribute('name');
        usersData = getDataObjArr(form);
        var postData = [
            {"key":"subject","value":nameMess},
            {"key":"dataString","value":getDataObjArr(form)}
        ];
        postForm(_createFormData(postData));
    }
    function htmlContains(classes){
        return document.documentElement.classList.contains(classes);
    }
    
    function validForm(form){
        var element = form.elements;
        for(var i = 0, len = element.length; i < len; i++){
            if(element[i].type == 'text' && element[i].name == 'Site'){
                continue;
            }
            if(element[i].type == 'text' && element[i].name == 'skills'){
                continue;
            }
            if(element[i].type == 'text' && element[i].name == 'Company'){
                continue;
            }
            if(element[i].type == 'text' && element[i].name == 'School'){
                continue;
            }
            if(element[i].type == 'text' && !isValidName(element[i].value)){
                element[i].classList.add('no-valid');
                if(element[i].classList.contains('commbobox__input')){
                    element[i].parentElement.classList.add('no-valid');
                }
            }else if(element[i].type == 'text' && isValidName(element[i].value)){
                element[i].classList.remove('no-valid');
                if(element[i].classList.contains('commbobox__input')){
                    element[i].parentElement.classList.remove('no-valid');
                }
            }
            if(element[i].type == 'tel' && !isVAlidPhone(element[i].value)){
                element[i].classList.add('no-valid');
            }else if(element[i].type == 'tel' && isVAlidPhone(element[i].value)){
                element[i].classList.remove('no-valid');
            }
            if(element[i].type == 'email' && !isValidEmailAddress(element[i].value)){
                element[i].classList.add('no-valid');
            }else if(element[i].type == 'email' && isValidEmailAddress(element[i].value)){
                element[i].classList.remove('no-valid');
            }
            if(element[i].type == 'checkbox' && !element[i].checked){
                element[i].nextElementSibling.classList.add('no-valid');
                element[i].classList.add('no-valid');
                if(element[i].classList.contains('commbobox__input')){
                    element[i].parentElement.classList.add('no-valid');
                }
            }else if(element[i].type == 'checkbox' && element[i].checked){
                element[i].nextElementSibling.classList.remove('no-valid');
                element[i].classList.remove('no-valid');
                if(element[i].classList.contains('commbobox__input')){
                    element[i].parentElement.classList.remove('no-valid');
                }
            }
        }
    }
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }
    function isValidName(name){
        var pattern = /^[a-zA-Zа-яА-Я'][a-zA-Zа-яА-Я-' ]+[a-zA-Zа-яА-Я']?$/u;
        return pattern.test(name);
    }
    function isVAlidPhone(phone){
        var pattern =  /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
        return pattern.test(phone);
    }
    //btn close form
    (function (){
        var btnCloseForm = document.querySelectorAll('.close-form');
        var btnBackForm = document.querySelectorAll('.bnt-back-form');
        for(var i = 0,len = btnBackForm.length; len > i;i++){
            btnBackForm[i].addEventListener('click',hideform);
        }
        for(var k = 0,lens = btnCloseForm.length; lens > k;k++){
            btnCloseForm[k].addEventListener('click',hideform);
        }  
    }());
    
    
    function resetValueForm(){
        var form = formSubmitItem.elements;
        for(var i = 0,len = form.length;i < len;i++){
            if(form[i].type == 'submit'){
                if(form[i].hasAttribute('disabled')){
                    form[i].removeAttribute('disabled');
                    form[i].classList.remove('disabled-btn');
                } 
                continue;
            }
            form[i].value = '';
        }
    }
    
    
    var customEvent,
        //form reg wrap
        formJunior = document.getElementById('form-junior'),
        formEpert = document.getElementById('form-expert'),
        formSchool = document.getElementById('form-it-school'),
        formComp = document.getElementById('form-company'),   
        //form mess 
        formMess = document.getElementById('form-mess'),
        fromMessError = document.getElementById('form-mess-no'),
        // form reg 
        formRegExp = document.getElementById('register-expert'),
        formRegComp = document.getElementById('regist-comp'),
        formRegJun = document.getElementById('register-junior'),
        formRegSchool = document.getElementById('regist-school'),
        formSelectReg = document.getElementById('select-reg'),
        // btn main
        btnMainReg = document.getElementById('regist-main'),
        btnMainRegFooter = document.getElementById('regist-main-foot'),
        // btnMainLogin = document.getElementById('btn-logIn'),
        //btn menu
        btnMenuReg = document.getElementById('menu-reg'),
        btnMenuLogin = document.getElementById('menu-log-in'),
        // cart btn 
        // var btnCartEpertReg = document.getElementById('btn-expert'),
        // var btnCartJobReg = document.getElementById('reg-btn-card'),
        // var btnCartComReg = document.getElementById('reg-comp'),
        // var btnCartSchoolReg = document.getElementById('btn-school-cart'),
        // blackout
        blackout = document.getElementById('blackout'),
        // попап письмо ушло или нет
        popUpMesGood = document.getElementById('pop-up-send-mes'),
        popUpMesError = document.getElementById('pop-up-no-send-mes'),
        
        // попап старт скоро
        popUpSoon = document.getElementById('pop-up-soon'),
        // btn contact-us
        contactBtn = document.getElementById('btn-contact-us'),
        // closeContactUs = document.getElementById('close-pop-up'),
        
        closePopUp = document.querySelectorAll('.close-popUp'),
        
        popUpCont = document.getElementById('pop-up-contact'),
        popUpContMenu = document.getElementById('contact-us-menu'),
        
        registForm = blackout.querySelectorAll('.register-form'),
        registFormAll = document.querySelectorAll('.register')
    
        popUpCont && popUpCont.addEventListener('submit',onsubmitHandler);
        
        advertising && advertising.addEventListener('submit',onsubmitHandlerAdverting);
    
        // анимация появления формы обратной свяязи
        contactBtn && contactBtn.addEventListener('click',showPopUpCont);
        popUpContMenu && popUpContMenu.addEventListener('click',showPopUpCont);
    
        document.getElementById('reg-soon').addEventListener('click',showBtnSoon);
        document.getElementById('cont-soon').addEventListener('click',showBtnSoon);
        // document.getElementById('menu_enty').addEventListener('click',showBtnSoonEntry);
        // btnMainLogin.addEventListener('click',showPopupSoon);
        btnMenuLogin && btnMenuLogin.addEventListener('click',showPopupSoon);
        btnMainReg && btnMainReg.addEventListener('click',showSelectReg);
        btnMainRegFooter && btnMainRegFooter.addEventListener('click',showSelectReg);
        btnMenuReg && btnMenuReg.addEventListener('click',showSelectReg);
        formSelectReg && formSelectReg.addEventListener('click',showFormMain);
    
    // wrap form reg
    document.getElementById('btn-expert') && document.getElementById('btn-expert').addEventListener('click',registExpert);
    document.getElementById('reg-btn-card') && document.getElementById('reg-btn-card').addEventListener('click',registJob);
    document.getElementById('reg-comp') && document.getElementById('reg-comp').addEventListener('click',registComp);
    document.getElementById('btn-school-cart') && document.getElementById('btn-school-cart').addEventListener('click',registSchool);
    
    
    function registExpert(){
        showBlackout();
        animForm(formEpert);
    }
    
    function registJob(){
        showBlackout();
        animForm(formJunior);
    }
    
    function registComp(){
        showBlackout();
        animForm(formComp);
    }
    
    function registSchool(){
        showBlackout();
        animForm(formSchool);
    }
    
    function showSelectReg(){
        showBlackout();
        if(htmlContains('mobile')){
            formSelectReg.classList.add('show-form');
        }else{
           if(blackout.classList.contains('blackout--show')){
                formSelectReg.classList.add('show-form');
                animForm(formSelectReg);
           }else{
            blackout.addEventListener('transitionend',showFormOpen);
            // eslint-disable-next-line no-inner-declarations
            function showFormOpen(){
                formSelectReg.classList.add('show-form');
                animForm(formSelectReg);
                blackout.removeEventListener('transitionend',showFormOpen);
            }
           }
        }
    }
    
    for(let i = 0,len = registForm.length; i < len; i++ ){
        registForm[i].addEventListener('submit',onsubmitHandler);
    }
    
    
    function hideAnimForm(select){
        let item =  null;
        
        for(let i = 0,len = registForm.length; i < len; i++ ){
            if(registForm[i].classList.contains('show-form')){
                item = registForm[i];  
            }
        }
    
        if(formSelectReg.classList.contains('show-form')){
            if(item == null){
                item = formSelectReg;
            }
        }
        if(item == null){
            for(let i = 0,len = registFormAll.length; i < len; i++ ){
                if(registFormAll[i].classList.contains('show-form')){
                    item = registFormAll[i];  
                }
            }
        }
    
    
        var duration = 200;
        new Animation(
        function(timePassed){
            var timeMuliplier = Animation.linear(duration, timePassed);
            item.style.right =  (-300 * timeMuliplier) + 'px';
        },
        duration,
        function() {
            item.classList.remove('show-form');
            if(select) {
                customEvent = new Event('anim-select-fihish');
                document.dispatchEvent(customEvent);
                return;
            } 
            hideBlackout();
        });
    }
    function animForm(item){
        var duration = 300;
        if(item.classList.contains('register')){
            item.classList.add('show-form');
        }
        item.firstElementChild.style.right ='-300px';
        new Animation(
        function(timePassed){
            var timeMuliplier = Animation.linear(duration, timePassed);
            item.style.right = -300 + (300 * timeMuliplier) + 'px';
        },
        duration,
        function() {
            if(item.querySelector('.wrap-form')){
                setTimeout(function(){
                animForm(item.querySelector('.wrap-form'));
                },100);
            }
        });
    }
    function showFormMain(event){
            var target = event.target;
            if(target.id.slice(0,4) != 'i_am') return;
    
            if(htmlContains('mobile')){
                delShowRegist();
                formSelectReg.classList.remove('show-form');
            }else{
                hideAnimForm(1);
            }
            if(target.id.slice(5) == 'junior'){
                formShowMainMob('junior');
            }else if(target.id.slice(5) == 'company'){
                formShowMainMob('company');
            }else if(target.id.slice(5) == 'expert'){
                formShowMainMob('expert');
            }else if(target.id.slice(5) == 'it-school'){
                formShowMainMob('it-school');  
            }
    }
    function delShowRegist(){
        for(var i = 0,len = registForm.length; i < len; i++ ){
            if(registForm[i].classList.contains('show-form')){
                registForm[i].classList.remove('show-form');
            }
        }
    }
    function formShowMainMob(item){
        if(htmlContains('mobile')){
            for(var i = 0,len = registForm.length; i < len; i++ ){
                if((registForm[i].id.slice(5) == item)){
                    registForm[i].classList.add('show-form');
                }
            }
        }else{
            var select = null;
            if(item == 'junior'){
                select = formJunior;
            }else if(item == 'company'){
                select = formComp;
            }else if(item == 'expert'){
                select = formEpert;
            }else if(item == 'it-school'){
                select = formSchool;
            }
            // eslint-disable-next-line no-inner-declarations
            function animFormSel(){
                setTimeout(function(){
                    animForm(select);
                },100);
                document.removeEventListener('anim-select-fihish',animFormSel);
            }
            document.addEventListener('anim-select-fihish',animFormSel);
        }
    }
    function hideform(){
        if(htmlContains('mobile')){
            if(event.target.hasAttribute('data-advent')){
                for(var k = 0,len = registFormAll.length; k < len; k++ ){
                    if(registFormAll[k].classList.contains('show-form')){
                        registFormAll[k].classList.remove('show-form');
                    }
                }
                advertising.classList.add('show-pop-up');
                return;
            }
            hideBlackout();
            // eslint-disable-next-line no-inner-declarations
            function animFormSel(){
                for(var i = 0,len = registFormAll.length; i < len; i++ ){
                    if(registFormAll[i].classList.contains('show-form')){
                        registFormAll[i].classList.remove('show-form');
                    }
                }
                blackout.removeEventListener('transitionend',animFormSel);
            }
            blackout.addEventListener('transitionend',animFormSel);
            return;
        }
        if(event.target.hasAttribute('data-advent')){
            hideAnimForm(1);
            document.addEventListener('anim-select-fihish',showAdver);
            // eslint-disable-next-line no-inner-declarations
            function showAdver(){
                formSubmitItem = advertising.querySelector('form');
                resetValueForm();
                showAdvertising();
                document.removeEventListener('anim-select-fihish',showAdver);
            }
            return;
        }
        hideAnimForm();
    }
    
    function showBlackout(){
        if(blackout.classList.contains('blackout--show')) return;
        blackout.classList.add('blackout--show');
    }
    function hideBlackout(){
        blackout.style.cssText = 'opacity:0; transition: opacity 0.250s linear;';
        blackout.addEventListener('transitionend',animBlack);
        function animBlack(){
                delShowRegist();
                blackout.classList.remove('blackout--show');
                if(!blackout.classList.contains('blacout--show')){
                    delStyle(blackout);
                    for(var i = 0,wrap = document.querySelectorAll('.wrap-form'),len = wrap.length; i < len;i++){
                        wrap[i].removeAttribute('style');
                    }
                }
                blackout.removeEventListener('transitionend',animBlack);    
            }
    }
    function messSent(res){
        hideAnimForm(1);
        
        document.addEventListener('anim-select-fihish',messAnim);
    
        function messAnim(){
            if(res){
                animForm(formMess);
            }else{
                animForm(fromMessError);
            }
            resetValueForm();
            document.removeEventListener('anim-select-fihish',messAnim);
        }
    }
    
    // function hidePopUpSend(){
    //     blackout.style.cssText = 'opacity:0; transition: opacity 0.250s linear;';
    //     setTimeout(function(){
    //         delStyle(popUpSendMes,blackout);
    //         blackout.classList.remove('blackout--show');
    //         popUpSendMes.classList.remove('show-pop-up');
    //     },250);		
    // }
    
    // function showPopUpSend(popUpActiv){
    
    //     if(popUpActiv){
    //         popUpNoSend.style.opacity = '0';
    //         popUpNoSend.classList.add('show-pop-up');
    //         setTimeout(function(){
    //             popUpNoSend.style.transition = 'opacity 0.3s linear';
    //             popUpNoSend.style.opacity = '1';
    //         },100);
    //     }else{
    //         popUpSendMes.style.opacity = '0';
    //         popUpSendMes.classList.add('show-pop-up');
    //         setTimeout(function(){
    //             popUpSendMes.style.transition = 'opacity 0.3s linear';
    //             popUpSendMes.style.opacity = '1';
    //         },100);
    //     }
        
    // }

    
    function showPopupSoon(){
        showPopUp(popUpSoon); 
    }
    function showBtnSoonEntry(){
        hideAnimForm(1);
        document.addEventListener('anim-select-fihish',showPopUpSoon);
    
        function showPopUpSoon(){
            showPopupSoon();
            document.removeEventListener('anim-select-fihish',showPopUpSoon);
        }
    }
    
    function showBtnSoon(){
        hidePopUp(popUpSoon,1);
    
        var self = this;
    
        document.addEventListener('anim-finish-pop-up',popUpShowMess);
    
        function popUpShowMess(){
            if(self.id == 'reg-soon'){
                blackout.style.transition = 'none';
                blackout.classList.remove('blackout--show');
                setTimeout(function(){
                    delStyle(blackout);
                    showSelectReg();
                },0);
            }else{
                showPopUp(popUpCont);
            }
            
            document.removeEventListener('anim-finish-pop-up',popUpShowMess);
        }
    }
    
    
    
    function showPopUpCont(){
        showPopUp(popUpCont);
    }
    
    function showPopUp(item){
        showBlackout();
        item.classList.add('show-pop-up');
        item.style.transform = 'translateY(30px)';
        item.style.opacity = '0';
        var duration = 300;
        new Animation(
            function(timePassed) {
                var timeMuliplier = Animation.linear(duration, timePassed);
                item.style.transform = 'translateY(' + (30 - (30 * timeMuliplier)) +'px)';
                item.style.opacity = timeMuliplier;
            },
            duration,
            function(){
    
        });
    }
    
    
    (function (){
        for(var i = 0,len = closePopUp.length;i < len;i++){
            closePopUp[i].addEventListener('click',hidePopUp);
        }
    })();
    
    // eslint-disable-next-line consistent-return
    function popUpActivNow(){
        for(var i = 0,len = closePopUp.length;i < len;i++){
            if(closePopUp[i].closest('.show-pop-up')){
                return closePopUp[i].closest('.show-pop-up');
            }
        }
    }
    
    function hidePopUp(item,all){
        item = popUpActivNow();
        var duration = 200;
        new Animation(
            function(timePassed) {
                var timeMuliplier = Animation.linear(duration, timePassed);
                item.style.transform = 'translateY(' +  (30 * timeMuliplier) +'px)';
                item.style.opacity = 1 - timeMuliplier;
            },
            duration,
            function(){
                item.classList.remove('show-pop-up');
                delStyle(item);
                if(all){
                    customEvent = new Event('anim-finish-pop-up');
                    document.dispatchEvent(customEvent);
                    return;
                }
                hideBlackout();
        });
    }
    
    
    function messSentPopUp(res){
        popUpactiv = false;
    
        hidePopUp(popUpCont,1);
    
        customEvent = new Event('anim-finish-pop-up');
        document.dispatchEvent(customEvent);
    
    
        document.addEventListener('anim-finish-pop-up',popUpShowMess);
    
        function popUpShowMess(){
            if(res){
                showPopUp(popUpMesGood);
            }else{
                showPopUp(popUpMesError);  
            }
            resetValueForm();
            document.removeEventListener('anim-finish-pop-up',popUpShowMess);
        }
    }
    
    //мобильная версия бороды 
    
    function showContactUs(){
        showBlackout();
        popUpCont.classList.add('show-pop-up');
    }
    
    
    // планшет и выше 
    // function showContactUs(){
    //     showBlackout();
    //     popUpCont.classList.add('show-pop-up');
    // };
    

    (function(){       
        var city = ['Киев','Харьков','Одесса','Львов','Донецк','Николаев','Мариуполь','Луганск','Запорожье','Днепр','Кривой Рог','Винница','Херсон','Полтава','Чернигов','Черкассы','Хмельницкий','Житомир','Черновцы','Сумы','Ровно','Ивано-Франковск','Каменское / Днепродзержинск','Кропивницкий / Кировоград','Кременчуг','Тернополь','Краматорск','Мелитополь','Ужгород','Никополь','Бердянск','Славянск','Павлоград','Северодонецк','Каменец-Подольский'],
         persone = ['Соискатель','Работодатель','Эксперт','IT Школа'],
         school = ['Ukrainian IT School','ruby','sourse'],
         combo = document.getElementById('combo'),
         combo2 = document.getElementById('combo2'),
         comboComp = document.getElementById('comboComp'),
         comboCont = document.getElementById('comboboxContact'),
         comboObj = {
            city: city,
            school: school
        };
        (function (){
            var combobox = document.querySelectorAll('.combobox');
            for(var i = 0,len = combobox.length; len > i; i++){
                combobox[i].addEventListener('click',dropComp);
            }
        }());


        function dropComp(event){
            let comboItem;
            if(this.id == 'combo'){
                comboItem = comboObj.city;
            }else if(this.id == 'combo2'){
                comboItem = comboObj.school;
            }else if(this.classList.contains('combobox-city')){
                comboItem = comboObj.city;
            }else if(this.id == 'comboboxContact'){
                comboItem = persone;
            }
            let target = event.target,
            self = this,

            comboInput = this.querySelector('.commbobox__input'),
            custLi = this.querySelector('.cust-li');

            comboInput.addEventListener('keyup', complite);
            this.classList.toggle('comb-open');
            if(this.classList.contains('combobox-city') && comboInput.value == ''){
                custLi.innerHTML = getComliteHtml(comboItem);	
            }
            if(this.id == 'comboboxContact'){
                custLi.innerHTML = getComliteHtml(comboItem);
            }
            if((combo.classList.contains('comb-open') && comboInput.value == '') || (combo.classList.contains('combobox-val') && comboInput.value == '') ){
                custLi.innerHTML = getComliteHtml(comboItem);
            }
            if(!target.classList.contains('cust__item')){
                return;
            }else{
                comboInput.value =  target.innerHTML;
                if(comboInput.value != ''){
                    self.classList.add('combobox-val');
                }
            }
            
            function complite() {
                var val = comboInput.value.trim().toLowerCase();	
                if (val) {
                    var words = comboItem.filter(function(item) {
                        return item.toLowerCase().indexOf(val) === 0;
                    });
                    custLi.innerHTML = getComliteHtml(words);
                }else{
                    custLi.innerHTML = getComliteHtml(comboItem);
                }
            }
            function getComliteHtml(words) {
                var html = "";
                for (var i = 0,len = words.length; i < len; i++) {
                    html += '<li class="cust__item">' + words[i] + '</li>';
                }
                return html;
            }
        }
    }());
    
    (function(){
        var citySchool = ['Киев','Харьков'],
         schoolKiev = ['Main Academy','Pause to play school','Foxminded','Jon','Hillel','UX-Answer','Beetroot Academy','BrainBasket','Шаг','SkillUp','Vertex academy','Perspectiva','Dialon'
        ,'Uspeh','CodeSpace','IT Education Center','Интеллект','QA Start Up','FullStack Академия','Учебный центр инноваций','Web Academy','GoIT','StudPoint','IIBT (Институт информационных и бизнес-технологий)','ITKurs','QALight','Clubkey','Artclub','ArtCraft'
        ,'ITEA (IT Education Academy)'],
         schoolKharkiv = ['Telesens Academy','A-Level','UKRAINIAN IT SCHOOL','SOURCE IT','Beetroot Academy','Проминь','Starindustry','Doctrina IT shool','Lemon school','BrainBasket','Сочник','IT Cloud Academy','Шаг','Hillel'
        ,'SkillUp','asfasfa','ITEA (IT Education Academy)','I-Marketing School','ConceptArt','Easy Code School','3DMaya'],
         comboItem = [],
         custLi = null;

        // function checkedItem(event) {
        //     var target = event.target;
        //     if(target.tagName != "LI") return;
        //     target.parentElement.previousElementSibling.value = target.innerHTML;
        // }


        // function dropComboAvance(target){
        //     var city = document.getElementById('city-school'),

        //      school = document.getElementById('list-school');

        //     if(target.name == 'city-school'){
        //         comboItem = citySchool;
        //         custLi = city.nextElementSibling;
        //         city.parentElement.classList.add('comb-open');
        //         complite(city);
        //     }else if(target.name == 'list-school'){
        //         if(city.value == 'Киев'){
        //             comboItem = schoolKiev;
        //         }else if(city.value == 'Харьков'){
        //             comboItem = schoolKharkiv;
        //         }else{
        //             return;
        //         }
        //         custLi = school.nextElementSibling;
        //         school.parentElement.classList.add('comb-open');
        //         complite(school);
        //     }else{
        //         return;
        //     }
        // }

        function complite(items) {
            if(items.target){
            items = items.target; 
            }
            var val = items.value.trim().toLowerCase();

            if (val) {
                var words = comboItem.filter(function(item) {
                    return item.toLowerCase().indexOf(val) === 0;
                });
                custLi.innerHTML = getComliteHtml(words);
            }else{
                custLi.innerHTML = getComliteHtml(comboItem);
            }
        }
        function getComliteHtml(words) {
            var html = "";
            for (var i = 0,len = words.length; i < len; i++) {
                html += '<li class="cust__item">' + words[i] + '</li>';
            }
            return html;
        }


        // function comboboxInput(event){
        //     dropComboAvance(event.target);
        // }
        // function comboboxInputBlur(event){
        //     setTimeout(function(){
        //         var city = document.getElementById('city-school');
        //         var school = document.getElementById('list-school');
        //         city.parentElement.classList.remove('comb-open');
        //         school.parentElement.classList.remove('comb-open');
        //     },150);
        // }
    }());
}
export default form