import partnersDescriptionCompany from "../database/parners/company.json"
import partnersDescriptionSchool from "../database/parners/school.json"
import partnersDescriptionLangSchool from "../database/parners/lengSchool.json"
import partnersDescriptionNonprofit from "../database/parners/noProfit.json"

import {
    createTag
} from "../helper/helper.js";

class PartnersFilter{
    constructor(filter,wrapPartners,tabs,partnersItem){
        this.BLockFilter = filter;
        this.BlockWrapPartners = wrapPartners;
        this.wrapPartners = document.getElementById(this.BlockWrapPartners);
        this.filter = document.getElementById(this.BLockFilter);
        this.partnersItem = partnersItem || {};
        if(this.filter){
            this.filter.addEventListener('click',(event) =>{
                let target = event.target;
                if(!target.hasAttribute('data-filter-tabs')) return;
                this.activeTabs(target); 
            });
        }
        this.config = {
            company: partnersDescriptionCompany,
            school: partnersDescriptionSchool,
            langSchool: partnersDescriptionLangSchool,
            noProfit: partnersDescriptionNonprofit
        };
        this.render()
    }

    appendNavTab(item){
        if(this.filter){
            this.filter.appendChild(item);
        }
    }

    activeTabs = target =>{
        if(target.classList.contains('block-partners__nav--active')) return;
        if(!document.documentElement.classList.contains('mobile')){
            let childFilter = this.filter.children;
            for(let i = 0,len = childFilter.length; i < len; i++){
                childFilter[i].classList.remove('block-partners__nav--active');
            }
            target.classList.add('block-partners__nav--active');
            if(target.getAttribute('data-filter-tabs') == 'company'){   
                this.appendPartners(partnersDescriptionCompany);
            }else if(target.getAttribute('data-filter-tabs') == 'school'){
                this.appendPartners(partnersDescriptionSchool);
            }else if(target.getAttribute('data-filter-tabs') == 'langSchool'){
                this.appendPartners(partnersDescriptionLangSchool);
            }else if(target.getAttribute('data-filter-tabs') == 'noProfit'){
                this.appendPartners(partnersDescriptionNonprofit);
            }
        }
    }

    filterParners(item,append){
        let wrap = createTag('div');
        wrap.className = "partners__item";
        wrap.classList.add(item.name);
        wrap.id = item.name;

        wrap.setAttribute('data-filter',item.filter);

        let wrapImg = createTag('div');
        wrapImg.className = "wrap-img";
        

        let logo = createTag('img');
        logo.className = item.classImg;
        logo.src = item.img;
        logo.alt = '';
        wrapImg.appendChild(logo);
        if(item.mobImg){
            let logoMob = createTag('img');
            logoMob.src = item.mobImg;
            logoMob.className = item.classMobImg;
            wrapImg.appendChild(logoMob);
            wrapImg.alt = '';
        }

        let wrapContent = createTag('div');

        wrapContent.className = "partners__content";

        if(item.imgDesc){
            let imgDesc = createTag('img');
            imgDesc.src = item.imgDesc;
            imgDesc.className =  item.imgDescClass;
            imgDesc.alt = '';
            wrapContent.appendChild(imgDesc);
        }

        let decsription = createTag('p');
        decsription.className ="modal-parners__desc-text";
        decsription.innerHTML = item.description;

        let infoLocation = createTag('p');
        infoLocation.className ="modal-parners__desc-city";
        if(item.personal){
            infoLocation.innerHTML = '<span class="d-b">'+item.personal+'</span>'+item.location;
        }else{
            infoLocation.innerHTML = item.location;
        }


        decsription.appendChild(infoLocation);
        let link = createTag('a');
        link.href = item.link;
        link.className = "link-partners";
        link.innerHTML = item.linkDesc;
        link.target = '_blank';

        wrapContent.appendChild(decsription);
        wrapContent.appendChild(link);

        wrap.appendChild(wrapImg);
        wrap.appendChild(wrapContent);

        append.appendChild(wrap);
        // drag(wrap);// включить выключить драгндроп
    }

    appendPartners(items){
        if(!document.documentElement.classList.contains('mobile')){
            this.wrapPartners.innerHTML = '';
        }else{
           this.allPartnersAppend();
           return;
        }
        items.forEach(item=>{
            this.filterParners(item, this.wrapPartners);
        })
    }
    allPartnersAppend(){
        this.partnersItem.forEach(item=>{
            if(item == "company"){
                partnersDescriptionCompany.forEach(items=>{
                    this.filterParners(items, this.wrapPartners);
                })
            }else if(item == "langSchool"){
                partnersDescriptionLangSchool.forEach(items=>{
                    this.filterParners(items, this.wrapPartners);
                })
            }else if(item == "school"){
                partnersDescriptionSchool.forEach(items=>{
                    this.filterParners(items, this.wrapPartners);
                })
            }else if(item == "noProfit"){
                partnersDescriptionNonprofit.forEach(items=>{
                    this.filterParners(items, this.wrapPartners);
                })
            }
        }) 
    }
    render(){
        this.appendPartners(this.config[this.partnersItem[0]]);
    }
}

export default class Partners {
    constructor(props){
        this.wrapPartners = props.wrapPartners && document.getElementById(props.wrapPartners);
        props.partnersblock && document.getElementById(props.partnersblock).addEventListener('click',this.openInfoParter);
        this.navBlock = props.navBlock && document.getElementById(props.navBlock) || null;
        this.navigation = props.navigation;
        this.blackOutBLock = createTag('div');
        this.descriptionFieldWrap = createTag('div');
        this.fielWrapper = createTag('div');
        this.descriptionField = createTag('div');
        this.self = this;
        this.animPartners = false;
        this.partnersItem = props.partnersItem;
        this.addClassBLock();
        this.appendItem();
        this.createNavTab();
        new PartnersFilter('block-partners__nav','partners-block',props.nav,props.partnersItem)
    }

    addClassBLock(){
        this.blackOutBLock.className = 'modal-parners';

        this.descriptionField.className = 'modal-parners__desc-field';

        this.fielWrapper.className = 'modal-parners__wrap-field';

        this.descriptionFieldWrap.className='modal-parners__img-wrap';
    }

    openInfoParter = (event = {}) =>{
        let target = event.target;
        if(target.closest('.partners__content')){
            return;
        }
        let item = target.closest('.partners__item');
    
        if(document.documentElement.classList.contains('mobile')){
            this.openPartPortaitMob(item);
            
            if(document.documentElement.classList.contains('landscape')){
                return;
            }
        }else{
            this.openPartPortaitIpad(item);
        }
    }

    openPartPortaitMob(target){
        if(!target)return
        if(target.classList.contains('open-cart-partners')){
            target.classList.remove('open-cart-partners');
        }else{
            target.classList.add('open-cart-partners');
        }
    }
    
    closeDescPArners  = (event = {}) =>{
        let target = event.target;
        if(!target.classList.contains('modal-parners')) return;
        this.blackOutBLock.classList.remove('modal-parners--open');
    }
    createNavTab(){
        for(let item in this.navigation){
            let span = createTag('span');
            if(this.partnersItem[0] == item){
                span.classList = 'block-partners__nav--active';
            }
            span.innerHTML = this.navigation[item];
            span.setAttribute('data-filter-tabs', item);
            this.navBlock.appendChild(span);
        }
    }
    openPartPortaitIpad(target){
        if(!target) return;
        this.animPartners = true;
        this.descriptionFieldWrap.innerHTML = '';
        this.descriptionField.innerHTML = '';
        let nodeImg = target.children[0].cloneNode(1);
        this.descriptionFieldWrap.appendChild(nodeImg);

        for(let i = 0; i < target.children[1].children.length; i++){
            let nodeDesk = target.children[1].children[i].cloneNode(1);
            this.descriptionField.appendChild(nodeDesk);
        }
        setTimeout(() =>{
            this.blackOutBLock.classList.add('modal-parners--open');
        },0);
    }

    appendItem(){
        this.fielWrapper.appendChild(this.descriptionFieldWrap);
        this.fielWrapper.appendChild(this.descriptionField);
        this.blackOutBLock.appendChild(this.fielWrapper);
        document.body.appendChild(this.blackOutBLock);
        this.blackOutBLock.addEventListener('click',this.closeDescPArners);
    }
}