export default class animate {

    constructor(options){
        this._options = options;
        this._state = 'play';
        this.play();
    }

    getStatus(){
        return this._state;
    }

    play(){
        this._start = performance.now();
        requestAnimationFrame(this.animate);
    }

    paused(){
        this._state = 'paused';
    }
    progress(){

    }
    animate = (time) =>{
        // debugger;
        // timeFraction от 0 до 1
        if(this._state == 'paused'){
            this.calcTimePaused(time);
            return;
        }
        let timeFraction = 1
        if(this._timeLeft) {
            timeFraction  = this._progress + ( / this._options.duration)
        }else{
            console.log(time, this._start);
            timeFraction  = (time - this._start) / this._options.duration;
        }

        if (timeFraction > 1) timeFraction = 1;
        
        // текущее состояние анимации
        let progress = this._options.timing(timeFraction)
        
        this._options.draw(progress);

        if (timeFraction < 1) {
            requestAnimationFrame(this.animate);
        }else{
            this._timeLeft = null;
            this._state = 'end';
        }
    }
    calcTimePaused(time){
        this._timeLeft = (this._options.duration - ((time - this._start) / this._options.duration) * this._options.duration);
        this._progress = (time - this._start) / this._options.duration;
    }
}