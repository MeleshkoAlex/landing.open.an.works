

export default function verticalTab(wrap){
    if(!wrap);
    let tabs = document.getElementById(wrap);
    if(!tabs) return;
    tabs.addEventListener('click',toogleTab);
    let main = document.querySelectorAll('.vertical-tab__desc');

    initMainDesc();

    function toogleTab(event){
        let target = event.target;
        if(!target.getAttribute('vertical-tab')) return;

        let descBlock = target.nextElementSibling;

        if(target.classList.contains('vertical-tab--open')){
            closeTab(descBlock);
            target.classList.remove('vertical-tab--open');
        }else{
            openTab(descBlock);
            target.classList.add('vertical-tab--open');
        }
    }

    function openTab(element){
        let height = element.getAttribute('data-height');
            element.style.height = `${height}px`;
    }
    function closeTab(element){
        element.style.height = '0px';
    }

    function initMainDesc(){
        if(!main) return;
        for(let item of main){
            let height = item.clientHeight;
            item.setAttribute('data-height',height);
            item.classList.add('vertical-tab__desc--anim');
        }
    }
}