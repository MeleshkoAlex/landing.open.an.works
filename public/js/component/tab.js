export default class Tab{
    constructor(options){
        this.optionsProp = options || {};
        this.options = {
            listBtn:'.tabs--btn',
            listBlock: '.tabs--block',
            attNavBtn: 'data-tab-nav',
            attBlock: 'data-tab-block',
            activeBtnClass: 'tabs-btn--active',
            activeBlockClass: 'tabs-block--active',
            captionTabName: '.tab--caption-tech'
        };
        this.init();
    }
    init(){
        this.initOptions();
        this.findDomItem();
        this.initBtn();
        this.chackActiveBLock();
    }
    findDomItem(){
        this.options.domListItem = document.querySelector(this.options.listBtn);
        this.options.domBlockItem = document.querySelector(this.options.listBlock);
        this.options.blocksItem = this.options.domBlockItem && this.options.domBlockItem.querySelectorAll(`[${this.options.attBlock}]`);
        this.options.btnNav = this.options.domListItem && this.options.domListItem.querySelectorAll(`[${this.options.attNavBtn}]`);
        this.options.domCaptionName = this.options.captionTabName && document.querySelector(this.options.captionTabName);
    }
    initOptions(){
        if(!Object.keys(this.optionsProp).length) return;
        for(let item in this.optionsProp){
            this.options[item] = this.optionsProp.item;
        }
    }
    initBtn(){
        if(!this.options.domListItem) return;
        this.options.domListItem.addEventListener('click',this.changeItem);
    }
    changeItem = (event)=>{
        let target = event.target;
        if(target.matches(`[${this.options.attNavBtn}]`)){
            let attributeBtn = target.getAttribute(this.options.attNavBtn);
            this.ctrlBLock(target,attributeBtn);
        }
    }
    ctrlBLock(elem,attr){
        if(!elem && !attr) return;
        this.deleteActiveClass();
        this.setActiveBtn(elem);
        let elemBLock = document.querySelector(`[${this.options.attBlock} = ${attr}]`);
        this.setActiveBLock(elemBLock);
        this.setHashBLock(attr);
        this.setNameTechnology(elem.innerText);
    }
    deleteActiveClass(){
        for(let elem of this.options.btnNav){
            elem.classList.remove(this.options.activeBtnClass);
        }
        for(let elem of this.options.blocksItem){
            elem.classList.remove(this.options.activeBlockClass);
        }
    }
    setActiveBtn(elem){
        if(!elem) return;
        elem.classList.add(this.options.activeBtnClass);
    }
    setActiveBLock(elem){       
        if(!elem) return;
        elem.classList.add(this.options.activeBlockClass);
    }
    chackActiveBLock(){
        let hash = location.hash && location.hash.slice(1);
        if(!hash){
            hash = this.options.btnNav[0].getAttribute(this.options.attNavBtn);
        }
        let activeBtn = document.querySelector(`[${this.options.attNavBtn} = ${hash}]`),
            activeBLock = document.querySelector(`[${this.options.attBlock} = ${hash}]`);
        if(!activeBtn){
            console.error(`${hash} button don't have elem in dom`);
        }
        if(!activeBLock){
            console.error(`${hash} block don't have elem in dom`);
        }
        this.setActiveBtn(activeBtn);
        this.setActiveBLock(activeBLock);
        this.setNameTechnology(activeBtn.innerText);
    }
    setHashBLock(hash){
        location.hash = `#${hash}`;
    }
    setNameTechnology(txt){
        this.options.domCaptionName.innerText = txt;
    }
}