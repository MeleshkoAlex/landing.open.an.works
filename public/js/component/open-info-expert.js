import {Animation} from "../common/anim.js";
import device from 'current-device';
export default function openInfoExpert(){
    let blocks = document.querySelector('.tabs--block');
    blocks && blocks.addEventListener('click',openInfo);
}

function openInfo(event){
    let target = event.target,
        element = target.closest('.expert__block') || target.matches('.expert__block');
    if(!element) return;
        if(element.classList.contains('expert--info-open')){
            if(device.type == 'mobile' || (device.type == 'tablet' && device.orientation == 'portrait')){
                hideInfoMob(element);
            }else{
                hideInfo(element);
            }
        }else {
            if(device.type == 'mobile' || (device.type == 'tablet' && device.orientation == 'portrait')){
                showInfoMob(element);
            }else{
                showInfo(element);
            }
        }    
}


function showInfoMob(element){
    setActiveClass(element);
}

function hideInfoMob(element){
    removeActiveClass(element);
}


function showInfo(element){
    let imgBLock = element.querySelector('.expert__wrap-img'),
    descBlock = element.querySelector('.expert__wrap-desc'),
    infoBlock = element.querySelector('.expert-information');
    setActiveClass(element);
    openImgBlock(imgBLock);
    openDescBlcok(descBlock).then(()=>{openInfoBLock(infoBlock);})
}

function hideInfo(element){
    let imgBLock = element.querySelector('.expert__wrap-img'),
    descBlock = element.querySelector('.expert__wrap-desc'),
    infoBlock = element.querySelector('.expert-information');
    removeActiveClass(element);
    hideInfoBlock(infoBlock).then(()=>{
        hideDescBlock(descBlock);
        hideImgBlock(imgBLock);
    });
}


function setActiveClass(elem){
    if(!elem) return;
    elem.classList.add('expert--info-open');
}

function removeActiveClass(elem){
    if(!elem) return;
    elem.classList.remove('expert--info-open');
}

// open anim block
function openDescBlcok(item,duration = 300){
    if(!item) return;
    // eslint-disable-next-line consistent-return
    return new Promise((res,rej)=>{
        new Animation(
            (timePassed) =>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                item.style.transform = 'translateX('+ (100 * timeMuliplier) + '%)';
                item.style.opacity = 1 - timeMuliplier;
            },duration,()=>{
                item.style.display = 'none';
                res();
            });
    });
}
function openInfoBLock(item,duration = 300){
    if(!item) return;
    item.style.transform = 'translateX(100%)';
    item.style.opacity = '0';
    item.style.display = 'flex';
    // eslint-disable-next-line consistent-return
    return new Promise((res,rej)=>{
        new Animation(
            (timePassed) =>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                item.style.transform = 'translateX('+ (100 - (100 * timeMuliplier)) + '%)';
                item.style.opacity = timeMuliplier;
            },duration,res);
    });   
}
function openImgBlock(item,duration = 300){
    if(!item) return;
    // eslint-disable-next-line consistent-return
    return new Promise((res,rej)=>{
        new Animation(
            (timePassed) =>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                item.style.width = `${50 - (10 * timeMuliplier)}%`
            },duration,res);
    });
}

//hide anim block
function hideDescBlock(item,duration = 300){
    if(!item) return;
    item.style.display = 'block';
    // eslint-disable-next-line consistent-return
    return new Promise((res,rej)=>{
        new Animation(
            (timePassed) =>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                item.style.transform = `translateX(${100 - (100 * timeMuliplier)}%)`;
                item.style.opacity = timeMuliplier;
            },duration,res);
    });
}

function hideInfoBlock(item,duration = 300){
    if(!item) return;
    // eslint-disable-next-line consistent-return
    return new Promise((res,rej)=>{
        new Animation(
            (timePassed) =>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                item.style.transform = 'translateX('+ (100 * timeMuliplier) + '%)';
                item.style.opacity = 1 - timeMuliplier;
            },duration,()=>{
                item.style.display = 'none';
                res();
            });
    });   
}
function hideImgBlock(item,duration = 300){
    if(!item) return;
    // eslint-disable-next-line consistent-return
    return new Promise((res,rej)=>{
        new Animation(
            (timePassed) =>{
                let timeMuliplier = Animation.linear(duration, timePassed);
                item.style.width = `${40 + (10 * timeMuliplier)}%`
            },duration,res);
    });
}
 