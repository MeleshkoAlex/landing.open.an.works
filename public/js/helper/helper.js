export const createTag = (item)=>{
    if(item && typeof(item) == "string"){
       return document.createElement(item);
    }
    return '';
}
export const hideScrollBody = () =>{
    document.body.style = "overflow: hidden;"
}
export const addScrollBody = () =>{
    document.body.style = "overflow: scroll;"
}
export const getCoords = (elem) =>{
    let box = elem.getBoundingClientRect();
    return {
        top: box.top,
        left: box.left
    }
}
export const delStyle = (...spred)=>{
    spred.forEach(item =>{
        item.removeAttribute('style');
    })
}
export const htmlContains = (classes) =>{
    return document.documentElement.classList.contains(classes);
}