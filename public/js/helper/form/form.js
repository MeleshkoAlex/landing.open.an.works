export let _createFormData = (valuesObj) => {
    let dataObjArr = valuesObj,
        formData = new FormData();
    for (let i = 0; i < dataObjArr.length; i++) {
        formData.append(dataObjArr[i].key, dataObjArr[i].value);
    }
    return formData;
};