import Cookie from "../modules/modal-cookis.js";
// import newMenu from "../modules/newMenu.js";
import menu from "../modules/menu.js";
import preload from "../modules/preloader.js";
import Tab from "../component/tab.js"
import form from '../modules/controllForm.js';
import openInfoExpert from '../component/open-info-expert.js';
export default class Company {
    constructor() {
        new Cookie();
        // new newMenu();
        new menu();
        preload();
        new Tab();
        openInfoExpert();
        form();
    }
}