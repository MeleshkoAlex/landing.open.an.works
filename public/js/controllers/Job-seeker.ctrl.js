import Cookie from "../modules/modal-cookis.js";
import Partners from "../modules/partners.js";
import AnchorScroll from "../modules/anchorScroll.js";
import Slider from "../modules/slider-functional.js";
import Menu from "../modules/menu.js";
import form from "../modules/controllForm.js";
import VerticalCarousel from "../modules/vertical-carousel.js";
import preload from "../modules/preloader.js";
export default class Applicant {
    constructor() {
        new Cookie();
        new Partners({
            wrapPartners:'wrapper-partner',
            partnersblock:'partners-block',
            navBlock:'block-partners__nav',
            partnersItem:['company'],
            navigation:{
                company: "IT company"
            }
        });
        new AnchorScroll({
            block:'nav-header',
            duration: 1000
        });
        new Slider({
            blockDots:'func-slider-dots',
            blockMain:'func-slider',
            blockCaption:'func-slider-caption',
            imgSlider: 'block-functional__slider-img'
        });
        new Menu();
        new VerticalCarousel({
            slider: "vertical-carousel",
            workspace: "vertical-carousel-workspace",
            nextBtn: "vertical-carousel-next-btn",
            prevBtn: "vertical-carousel-prev-btn",
            content: "vertical-carousel-content",
            decision: "block-why-we-desc__level-desc",
            number: "block-why-we-desc__number-item",
            caption: "block-why-we-desc__caption-level"
        })
        form();
        preload();
    }
}