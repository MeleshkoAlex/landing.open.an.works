class Router {
    static routes = {
        main: {
            path: new RegExp('^/$'),
            controller: 'applicant_registration'
        },
        company:{
            path: new RegExp('^/IT-employer.html'),
            controller: 'IT-employer'
        },
        applicant:{
            path: new RegExp('^/Job-seeker.html'),
            controller: 'Job-seeker'
        },
        recruiter:{
            path: new RegExp('^/recruiter_registration.html'),
            controller: 'recruiter_registration'
        },
        faq:{
            path: new RegExp('^/Faq.html'),
            controller: 'faq'
        },
        experts:{
            path: new RegExp('^/Experts.html'),
            controller: "experts"
        }
    }

    static getController(fullPath){
        let Controller = null;

        for (let key in Router.routes) {
            let {path, controller} = Router.routes[key];
            if(path instanceof RegExp && path.test(fullPath)){
                Controller = controller;
                break;
            }
        }

        return Controller;
    }
}

export default Router;