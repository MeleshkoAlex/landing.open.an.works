import __languages from './lang/lang.js'

function TranslationObserver(){
    this.__observers = [];
}

TranslationObserver.prototype.subscribe = function(fn) {
    this.__observers.push(fn)
}

TranslationObserver.prototype.unsubscribe = function (fn) {
    this.__observers = this.__observers.filter(subscriber => subscriber !== fn)
}

TranslationObserver.prototype.broadcast = function (data) {
    this.__observers.forEach(subscriber => subscriber(data))
}

Translation.prototype = Object.create(TranslationObserver.prototype);
Translation.prototype.constructor = Translation;


function Translation(props){
    TranslationObserver.call(this);
    this.optionsProps = props;
    this.stateLang = 'ru';
    this.options = {
        availableLanguage: ["ru","uk","en","pl"],
        mainBlock: 'wrapper-main',
        block: 'data-translation',
        placeholder: "data-translation-holder",
        attrValue: "data-translation-value",
        defaultLanguage: 'ru',
        languageSelected: 'ru'
    }
    this._init();
}

Translation.prototype._init = function(){
    if(this.optionsProps && Object.keys(this.optionsProps).length){
        for(var item in this.optionsProps){
            this.options[item] = this.optionsProps[item];
        }
    }
    this.mainBlock = document.getElementById(this.options.mainBlock);
    this._setTranslationLanguage(this._BrowserLang());
}
Translation.prototype._BrowserLang = function(){
    var localStorageLanguage = localStorage.getItem('defaultLanguage'),
        navigationLanguage = navigator.language,
        indexLanguage = navigationLanguage && navigationLanguage.indexOf('-'),
        defaultLanguage = this.options.defaultLanguage,
        key = null,
        lang = null;

    if(navigationLanguage){
        key = indexLanguage > -1 ? navigationLanguage.slice(0, indexLanguage) : navigationLanguage;
    }
    if(localStorageLanguage && localStorageLanguage == this.options.defaultLanguage){
        this._initControls(this.options.defaultLanguage);
        return;
    }else if(localStorageLanguage && localStorageLanguage != this.options.defaultLanguage){
        lang = this.options.availableLanguage.indexOf(localStorageLanguage) > -1 ? localStorageLanguage : this.options.defaultLanguage;
        localStorage.setItem('defaultLanguage',lang);
        this._initControls(lang);
        // eslint-disable-next-line consistent-return
        return lang;
    }
   
    if(navigationLanguage){
        lang = this.options.availableLanguage.indexOf(key) > -1 ? key : null;
        localStorage.setItem('defaultLanguage',lang ? lang : defaultLanguage);
        this._initControls(lang);
        // eslint-disable-next-line consistent-return
        return lang;
    }
}
Translation.prototype._findDomTranslationELement = function(){
    if(!this.options.block){
        console.error('Please specify attribute');
        return;
    }
    // eslint-disable-next-line consistent-return
    return this.mainBlock && this.mainBlock.querySelectorAll('*['+this.options.block+']') || document.querySelectorAll('*['+this.options.block+']');
}

Translation.prototype._findDomTranslationPlceholder = function(){
    if(!this.options.placeholder){
        console.error('Please specify attribute placeholder');
        return;
    }
    // eslint-disable-next-line consistent-return
    return this.mainBlock && this.mainBlock.querySelectorAll('*['+this.options.placeholder+']') || document.querySelectorAll('*['+this.options.placeholder+']');
}

Translation.prototype._findDomTranslationAttrValue = function(){
    if(!this.options.attrValue){
        console.error('Please specify attribute attrValue');
        return;
    }
    // eslint-disable-next-line consistent-return
    return this.mainBlock && this.mainBlock.querySelectorAll('*['+this.options.attrValue+']') || document.querySelectorAll('*['+this.options.attrValue+']');
}

Translation.prototype.changeLanguage = function(language){
    localStorage.setItem('defaultLanguage',language);
    this._setTranslationLanguage(language);
    this.broadcast(language);
}

Translation.prototype._setTranslationLanguage = function(language){
    this.stateLang = language ? language : this.options.defaultLanguage;
    if(!language) return;
    
    var elements = this._findDomTranslationELement(),
        placeholder = this._findDomTranslationPlceholder(),
        attrValues = this._findDomTranslationAttrValue();
    if(elements){
        for(var i = 0; i < elements.length; i++){
            elements[i].textContent = __languages[language][elements[i].dataset.translation];
        }    
    }
    if(placeholder){
        for(var k = 0; k < placeholder.length; k++){
            placeholder[k].placeholder = __languages[language][placeholder[k].dataset.translationHolder];
        }
    }
    if(attrValues){
        for(var j = 0; j < attrValues.length; j++){
            attrValues[j].value = __languages[language][attrValues[j].dataset.translationValue];
        }
    }
    document.documentElement.lang = language;
}

Translation.prototype._initControls = function(lang){
    var controlsPanel = document.querySelectorAll('.controls-lang'),
        self = this;
    if(!controlsPanel) return;

    var select = document.querySelectorAll('.controls-lang__select');
    
    initselect();

    function initselect(){
        for(var i = 0, len = select.length; i < len; i++){
            select[i].addEventListener('click',function(){
                toggleList(this.nextElementSibling);
            });
        }
    }


    for(var i = 0, len = controlsPanel.length; i < len; i++){
        var list = controlsPanel[i].querySelector('.controls-lang__list');
        if(!select && !list) continue;
        initcontrl(list);
    }

    function initcontrl(list){
        replaceClass(lang);
        
        list.addEventListener('click',function(event){
            var target = event.target;
            if(!target.matches('[data-lang]')) return;
            var lang = target.dataset.lang;
            self.changeLanguage(lang);
            replaceClass(lang);
            closeList(this);
        });
    }
    
    function replaceClass(langs){
        if(!select) return;
        for(var i = 0,len = select.length;i < len; i++){
            select[i].className = "controls-lang__select flag flag--phone-" + langs;
        }
    }
    function closeList(lists){
        lists.classList.remove('controls-lang__list--open');
    }
    function toggleList(lists){
        lists.classList.toggle('controls-lang__list--open');
    }
}

export default Translation;